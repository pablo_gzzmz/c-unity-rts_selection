
using UnityEngine;
using UnityEngine.UI;
using System;

[RequireComponent(typeof(Canvas))]
public class HUD : Singleton<HUD> {

    [SerializeField]
    private Transform worldBound;
    public  Transform WorldBound { get { return worldBound; } }

    [SerializeField]
    private new Camera camera;
    public      Camera Camera { get { return camera; } }

    private static Canvas canvas;
    public  static Canvas Canvas { get { return canvas; } }

    public static event Action OnDestroyEvent;

    protected override void SingletonAwake() {
        canvas = GetComponent<Canvas>();

        worldBound.SetAsFirstSibling();
    }

    /// <summary>
    /// Add transforms that are dependant on world positions.
    /// </summary>
    public static void AddWorldBound(Transform t) {
        t.SetParent(instance.worldBound);
        t.gameObject.layer = instance.worldBound.gameObject.layer;
    }

    private void OnDestroy() {
        if (OnDestroyEvent != null) OnDestroyEvent.Invoke();
    }
}
