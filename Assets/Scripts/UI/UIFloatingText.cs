
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[DisallowMultipleComponent]
[RequireComponent(typeof(RectTransform), typeof(Text))]
public class UIFloatingText : MonoBehaviour {
    private Text text;
    public  Text Text { get { return text; } }

    private Vector3 position = Vector3.zero;
    public  Vector3 Position { get { return position; }  set { position = value; } }

    private Vector3 floatDirection = Vector3.up;
    public  Vector3 FloatDirection { get { return floatDirection; } set { floatDirection = value; } }

    private float floatSum = 0;

    private float lifeTime;
    private float fadeTime;
    private float floatingSpeed;

    private void Awake() {
        text = GetComponent<Text>();
        
        HUD.AddWorldBound(transform);
        transform.position = new Vector3(-9999, -9999);
    }

    private void Update() {
        Vector3 pos = Camera.main.WorldToScreenPoint(position) + floatDirection * floatSum;
        pos.x /= HUD.Canvas.scaleFactor;
        pos.y /= HUD.Canvas.scaleFactor;
        pos.x = Mathf.Round(pos.x);
        pos.y = Mathf.Round(pos.y);
        pos.z = 0;
        transform.localPosition = pos;

        floatSum += Time.unscaledDeltaTime * floatingSpeed;

        lifeTime -= Time.unscaledDeltaTime;

        if (lifeTime <= fadeTime) 
        {
            float t = lifeTime / fadeTime;

            Color c = text.color;
            c.a = Mathf.Lerp(0, 1, t);
            text.color = c;

            if (t <= 0)
                Destroy(gameObject);
        }
    }

    public static UIFloatingText Log(string _string, Vector3 _worldPos, Color color, 
                                     float _lifeTime = 1.5f, float _floatingSpeed = 50, float _fadeTime = 0.5f) {
        GameObject go = Instantiate(Resources.Load("UI/FloatingText"), HUD.Instance.WorldBound) as GameObject;
        UIFloatingText floatingText = go.AddComponent<UIFloatingText> ();

        floatingText.text.text     = _string;
        floatingText.text.color    = color;
        floatingText.lifeTime      = _lifeTime;
        floatingText.floatingSpeed = _floatingSpeed;
        floatingText.fadeTime      = _fadeTime;
        floatingText.position      = _worldPos;

        return floatingText;
    }
}
