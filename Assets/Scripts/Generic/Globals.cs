
using UnityEngine;

public static class Globals {

    public const int playerMax = 4;
    
    public const int selectionPriorities = 8;
    public const int selectedMax = 20;
    public const int selectedMaxIndex = selectedMax - 1;

    public static readonly Color selectionColor = Color.green;

    public static readonly Color[] selectionPriorityColors = new Color[selectionPriorities]{
        Color.green,
        Color.green,
        Color.cyan,
        Color.cyan,
        Color.yellow,
        Color.yellow,
        Color.red,
        Color.red
    };

    static Globals() {
    }
}
