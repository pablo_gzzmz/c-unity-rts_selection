
using UnityEngine;

/// <summary>
/// Classes to easily handle 'attachment' to a player for this simple project.
/// </summary>

public abstract class PlayerBoundClass {

    protected Player player;
    public    Player Player { get { return player; } }

    public PlayerBoundClass(int _playerIndex) {
        player = Player.Get(_playerIndex);
    }

    public void SetPlayer(int _playerIndex) {
        player = Player.Get(_playerIndex);
    }

    public void SetPlayer(Player _player) {
        player = _player;
    }
}

public abstract class PlayerBoundComponent : MonoBehaviour {

    protected Player player;
    public    Player Player { get { return player; } }

    public void SetPlayer(int _playerIndex) {
        player = Player.Get(_playerIndex);
    }

    public void SetPlayer(Player _player) {
        player = _player;
    }
}

public abstract class PlayerBoundSingleton<T> : Singleton<T> where T : MonoBehaviour {

    protected static Player player;
    public    static Player Player { get { return player; } }

    public static void SetPlayer(int _playerIndex) {
        player = Player.Get(_playerIndex);
    }
    
    public static void SetPlayer(Player _player) {
        player = _player;
    }
}

