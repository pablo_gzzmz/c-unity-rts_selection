
using UnityEngine;
using System.Collections.Generic;

namespace GLPainting {

    enum LineMode { BasicLines, QuadLines }
    
    /// <summary>
    /// Class to control basic line drawing.
    /// <para>
    /// This script has been created as a simple abstraction of basic functionalities pertaining to Unity's GL library.
    /// </para>
    /// In this project, it is used to easily showcase the box selection through simple pixel perfect visuals.
    /// </summary>

    public class GLPainter : Singleton<GLPainter> {
        
        private static Material mat;

        private static float quadLineHeight, quadLineWidth;

        private static int pixelScale = 1;
        public  static int PixelScale {
            get { return pixelScale; }
            set {
                pixelScale = value < 1 ? 1 : value;
                pixelScaleMult = 1 / (float)pixelScale;
                halfPixelScale = pixelScale * 0.5f;
                Setlinesize();
            }
        }
        private static float pixelScaleMult = 1 / (float)pixelScale;
        private static float halfPixelScale;
        
        /// <summary> Viewport size of a pixel. </summary>
        private static Vector2 pixelSize = Vector2.zero;
        
        private static List<GLDrawLine> lines = new List<GLDrawLine>();
        public  static int LineCount { get { return lines.Count; } }

        private static LineMode lineMode = LineMode.QuadLines;

        private void Reset() { 
            // For camera components I prefer this method rather than RequireComponent.
            if (GetComponent<Camera>() == null) {
                Debug.LogError("This component should be placed only in a gameobject with a camera. Deleting.");
                UnityEditor.EditorApplication.delayCall += () => {
                    DestroyImmediate(this);
                };
            }
            SetMaterial();
        }

        private void Awake() {
            SetMaterial();

            ScreenController.OnScreenSizeChange += Setlinesize;
            Setlinesize();
        }

        private void OnPostRender() {
            if (lines.Count > 0) {
                mat.SetPass(0);
                GL.LoadOrtho();

                DrawLines();
            }
        }
        
        private static void SetMaterial() {
            if (mat == null) {
                Shader shader = Shader.Find("Hidden/Internal-Colored");
                Debug.Assert(shader != null);

                mat = new Material(shader);
                mat.hideFlags = HideFlags.HideAndDontSave;
                mat.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                mat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                mat.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
                mat.SetInt("_ZWrite", 0);
            }

            Debug.Assert(mat != null);
        }

        private static void Setlinesize() {
            quadLineWidth  = (float)(1 / (double)Screen.width)  * pixelScale;
            quadLineHeight = (float)(1 / (double)Screen.height) * pixelScale;

            pixelSize.x = quadLineWidth * pixelScaleMult;
            pixelSize.y = quadLineHeight * pixelScaleMult;
        }

        private static void DrawLines() {
            Color   prevColor = Color.white;
            Vector3 cachedV3  = Vector3.zero;

            for (int i = 0; i < lines.Count; i++) {
                if (!lines[i].draw) continue;

                switch (lines[i].ThisCategory) {
                    case GLDrawLine.Category.Line:
                        switch (lineMode) {
                            #region Basic lines
                            case LineMode.BasicLines:
                                GL.Begin(GL.LINES);
                                if (lines[i].LineColor != prevColor) {
                                    prevColor = lines[i].LineColor;
                                    GL.Color(lines[i].LineColor);
                                }

                                cachedV3 = lines[i].Start;
                                GL.Vertex(cachedV3);
                                cachedV3 = lines[i].End;
                                GL.Vertex(cachedV3);
                                break;
                            #endregion

                            #region Quad lines
                            case LineMode.QuadLines:
                                GL.Begin(GL.QUADS);
                                Vector3 direction = Vector3.zero;
                                float multiplierX = pixelSize.x * Mathf.Floor(halfPixelScale);
                                float multiplierY = pixelSize.y * Mathf.Floor(halfPixelScale);
                                float[] sum = { 0, 0 };

                                if ((pixelScale & 1) == 0) {
                                    if (lines[i].LineColor != prevColor) {
                                        prevColor = lines[i].LineColor;
                                        GL.Color(lines[i].LineColor);


                                        direction = (lines[i].End - lines[i].Start).normalized;

                                        // (x, y) rotated 90 = (-y, x)
                                        sum[0] = -direction.y * multiplierX;
                                        sum[1] = direction.x * multiplierY;
                                        float[] evenSum = { sum[0] + pixelSize.x, sum[1] - pixelSize.y };

                                        cachedV3.x = lines[i].Start.x + evenSum[0];
                                        cachedV3.y = lines[i].Start.y + evenSum[1];
                                        GL.Vertex(cachedV3);
                                        cachedV3.x = lines[i].Start.x - sum[0];
                                        cachedV3.y = lines[i].Start.y - sum[1];
                                        GL.Vertex(cachedV3);
                                        cachedV3.x = lines[i].End.x - sum[0];
                                        cachedV3.y = lines[i].End.y - sum[1];
                                        GL.Vertex(cachedV3);
                                        cachedV3.x = lines[i].End.x + evenSum[0];
                                        cachedV3.y = lines[i].End.y + evenSum[1];
                                        GL.Vertex(cachedV3);
                                    }
                                }
                                else {
                                    if (lines[i].LineColor != prevColor) {
                                        prevColor = lines[i].LineColor;
                                        GL.Color(lines[i].LineColor);
                                    }

                                    direction = (lines[i].End - lines[i].Start).normalized;

                                    // (x, y) rotated 90 = (-y, x)
                                    sum[0] = -direction.y * multiplierX;
                                    sum[1] = direction.x * multiplierY;
                                    cachedV3.x = lines[i].Start.x + sum[0];
                                    cachedV3.y = lines[i].Start.y + sum[1];
                                    GL.Vertex(cachedV3);
                                    cachedV3.x = lines[i].Start.x - sum[0];
                                    cachedV3.y = lines[i].Start.y - sum[1];
                                    GL.Vertex(cachedV3);
                                    cachedV3.x = lines[i].End.x - sum[0];
                                    cachedV3.y = lines[i].End.y - sum[1];
                                    GL.Vertex(cachedV3);
                                    cachedV3.x = lines[i].End.x + sum[0];
                                    cachedV3.y = lines[i].End.y + sum[1];
                                    GL.Vertex(cachedV3);
                                }
                                break;
                                #endregion
                        }
                        break;

                    // Automatically draws a box using the vectors of a single line
                    case GLDrawLine.Category.Box:
                        switch (lineMode) {
                            #region Basic lines
                            case LineMode.BasicLines:
                                GL.Begin(GL.LINES);
                                if (lines[i].LineColor != prevColor) {
                                    prevColor = lines[i].LineColor;
                                    GL.Color(lines[i].LineColor);
                                }

                                cachedV3 = lines[i].Start;
                                GL.Vertex(cachedV3);
                                cachedV3.y = lines[i].End.y;
                                GL.Vertex(cachedV3);
                                GL.Vertex(cachedV3);
                                cachedV3.x = lines[i].End.x;
                                GL.Vertex(cachedV3);
                                GL.Vertex(cachedV3);
                                cachedV3.y = lines[i].Start.y;
                                GL.Vertex(cachedV3);
                                GL.Vertex(cachedV3);
                                GL.Vertex(lines[i].Start);
                                break;
                            #endregion

                            #region Quad lines
                            case LineMode.QuadLines:
                                GL.Begin(GL.QUADS);

                                if (lines[i].LineColor != prevColor) {
                                    prevColor = lines[i].LineColor;
                                    GL.Color(lines[i].LineColor);
                                }

                                //First quad TOP LEFT-> TOP RIGHT
                                cachedV3 = lines[i].Start;
                                GL.Vertex(cachedV3);
                                cachedV3.x = lines[i].End.x - quadLineWidth;
                                GL.Vertex(cachedV3);
                                cachedV3.y = lines[i].Start.y - quadLineHeight;
                                GL.Vertex(cachedV3);
                                cachedV3.x = lines[i].Start.x;
                                GL.Vertex(cachedV3);

                                //Second quad TOP LEFT-> DOWN LEFT
                                GL.Vertex(cachedV3);
                                cachedV3.y = lines[i].End.y;
                                GL.Vertex(cachedV3);
                                cachedV3.x = lines[i].Start.x + quadLineWidth;
                                GL.Vertex(cachedV3);
                                cachedV3.y = lines[i].Start.y - quadLineHeight;
                                GL.Vertex(cachedV3);

                                //Third quad DOWN LEFT -> DOWN RIGHT
                                cachedV3.y = lines[i].End.y + quadLineHeight;
                                GL.Vertex(cachedV3);
                                cachedV3.x = lines[i].End.x;
                                GL.Vertex(cachedV3);
                                cachedV3.y = lines[i].End.y;
                                GL.Vertex(cachedV3);
                                cachedV3.x = lines[i].Start.x + quadLineWidth;
                                GL.Vertex(cachedV3);

                                //Fourth quad DOWN RIGHT -> TOP RIGHT
                                cachedV3.x = lines[i].End.x - quadLineWidth;
                                cachedV3.y = lines[i].End.y + quadLineHeight;
                                GL.Vertex(cachedV3);
                                cachedV3.x = lines[i].End.x;
                                GL.Vertex(cachedV3);
                                cachedV3.y = lines[i].Start.y;
                                GL.Vertex(cachedV3);
                                cachedV3.x = lines[i].End.x - quadLineWidth;
                                GL.Vertex(cachedV3);
                                break;
                                #endregion
                        }
                        break;

                    // Automatically fills a box as a quad using the vectors of a single line
                    case GLDrawLine.Category.Quad:
                        GL.Begin(GL.QUADS);

                        if (lines[i].LineColor != prevColor) {
                            prevColor = lines[i].LineColor;
                            GL.Color(lines[i].LineColor);
                        }

                        cachedV3 = lines[i].Start;
                        GL.Vertex(cachedV3);
                        cachedV3.x = lines[i].End.x;
                        GL.Vertex(cachedV3);
                        cachedV3.y = lines[i].End.y;
                        GL.Vertex(cachedV3);
                        cachedV3.x = lines[i].Start.x;
                        GL.Vertex(cachedV3);
                        break;
                }
                GL.End();
            }
        }
        
        public  static void AddLine   (GLDrawLine line) { lines.Add   (line); }
        
        public  static void DeleteLine(GLDrawLine line) { lines.Remove(line); }

        public  static void ClearAll() { lines.Clear(); }
    }
}
