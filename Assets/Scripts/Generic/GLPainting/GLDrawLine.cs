
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GLPainting {

    /// <summary>
    /// Class used to create drawing lines that will be used by the GLPainter.
    /// <para>
    /// Boxes and quads can be created automatically with a single of these lines via specifying Visual Data Type enum.
    /// </para>
    /// </summary>
    
    public class GLDrawLine {
        private Vector2 start;
        public  Vector2 Start { get { return start; } }

        private Vector2 end;
        public  Vector2 End { get { return end; } }

        private Color lineColor;
        public  Color LineColor { get { return lineColor; } }

        public bool draw = true;

        public  enum Category { Line, Box, Quad }
        private      Category category;
        public       Category ThisCategory { get { return category; } }

        public GLDrawLine() { start = Vector2.zero; end = Vector2.one; lineColor = Color.red; category = Category.Line; }

        public GLDrawLine(Vector2 v, Vector2 v2, Color col, Category _type) {
            start = v;
            end = v2;
            lineColor = col;
            category = _type;
        }

        public void SetType(Category _type) {
            category = _type;
        }

        public void SetColor(Color col) {
            lineColor = col;
        }

        public void WorldSpaceSet(Vector3 v, Vector3 v2) {
            ScreenSpaceSet(Camera.main.WorldToScreenPoint(v), Camera.main.WorldToScreenPoint(v2));
        }

        public void ScreenSpaceSet(Vector2 v, Vector2 v2) {
            FixBoxlinesPoints(ref v, ref v2);

            start = v;
            start.x = v.x / Screen.width;
            start.y = v.y / Screen.height;
            end = v2;
            end.x = v2.x / Screen.width;
            end.y = v2.y / Screen.height;
        }

        public void ViewportSpaceSet(Vector2 v1, Vector2 v2) {
            FixBoxlinesPoints(ref v1, ref v2);
            start = v1;
            end   = v2;
        }
        
        // Fixed to always have 'start' at the top left and 'end' at the bottom right.
        private void FixBoxlinesPoints(ref Vector2 start, ref Vector2 end) {
            if (category == Category.Line) return;

            float temp;
            if (start.y < end.y) {
                temp = start.y;
                start.y = end.y;
                end.y = temp;
            }
            if (start.x > end.x) {
                temp = start.x;
                start.x = end.x;
                end.x = temp;
            }
        }
        
        private void FixBoxlinesPoints(ref Vector3 start, ref Vector3 end) {
            if (category == Category.Line) return;

            float temp;
            if (start.y < end.y) {
                temp = start.y;
                start.y = end.y;
                end.y = temp;
            }
            if (start.x > end.x) {
                temp = start.x;
                start.x = end.x;
                end.x = temp;
            }
        }
    }
}