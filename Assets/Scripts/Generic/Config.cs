
using UnityEngine;

public static class Config {
    
    private static ConfigProfile profile;
    
    public static float DoubleClickTime { get { return profile.doubleClickTime; } }

    static Config() {
        profile = Resources.Load<ConfigProfile>("Profiles/Config Profile");
    }
}
