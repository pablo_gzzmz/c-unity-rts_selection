
using UnityEngine;
using System;

/// <summary>
/// Serialized options for the player. For this project only double click threshold is relevant.
/// </summary>

[CreateAssetMenu(fileName = "Players Profile", menuName = "Profiles/Players Profile")]
public class PlayersProfile : ScriptableObject {

    [Serializable]
    public struct PlayerInfo {
        public Color color;
        public int   team;

        public PlayerInfo(Color _color, int _team) {
            color = _color;
            team  = _team;
        }
    }

    [SerializeField]
    private PlayerInfo[] playerInfo = new PlayerInfo[Globals.playerMax+1];

    // Easy access via [] operator
    public  PlayerInfo this[int index] { get { return playerInfo[index]; } }

    public event Action OnValidateCompleted;

    public void OnValidate() {
        PlayerInfo[] temp = playerInfo;

        // Assert correct array
        playerInfo = new PlayerInfo[Globals.playerMax+1];

        for (int i = 0; i <= Globals.playerMax; i++) {
            if (i < temp.Length) {
                playerInfo[i] = temp[i];
            }

            else {
                playerInfo[i].color = Color.white;
                playerInfo[i].team  = i;
            }

            playerInfo[i].color.a = 1;

            if (i == 0) playerInfo[i].team = 0;
            else playerInfo[i].team = Mathf.Clamp(playerInfo[i].team, 1, Globals.playerMax);
        }

        if(OnValidateCompleted != null) OnValidateCompleted.Invoke();
    }
}
