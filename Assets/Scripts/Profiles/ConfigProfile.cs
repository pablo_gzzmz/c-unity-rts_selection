
using UnityEngine;

/// <summary>
/// Serialized options for the player. For this project only double click threshold is relevant.
/// </summary>

[CreateAssetMenu(fileName = "Config Profile", menuName = "Profiles/Config Profile")]
public class ConfigProfile : ScriptableObject {
    
    [Range(0.1f, 0.4f)]
    public float doubleClickTime = 0.2f;
}
