
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

/// <summary>
/// Simple scene initializator for the main controller. 
/// <para>
/// This component should be part of the Main Controller, and thus ensure persistency through scenes.
/// </para>
/// </summary>

public class InitializationController : Singleton<InitializationController> {

    // Initialization order
   enum GameplayInitialization {
        Players
        // ...
    }

    private static bool gameInitialized = false;
    public  static bool GameInitialized { get { return gameInitialized; } }
        
    public static event Action OnGameplayInitialization;
    
    protected override void SingletonAwake() {
        DontDestroyOnLoad(gameObject);

        GetComponentInChildren<EventSystem>().enabled = true;
        GetComponentInChildren<StandaloneInputModule>().enabled = true;

        SceneManager.activeSceneChanged += EvaluateNewScene;
    }

    private void EvaluateNewScene(Scene previous, Scene active) {
        if (active.name.Contains("gameplay")) InitializeGameplay();
    }
    
    private void InitializeGameplay() {
        gameInitialized = false;

        // Order of initialization is established by enum declaration
        foreach (GameplayInitialization type in Enum.GetValues(typeof(GameplayInitialization))) {
            switch (type) {
                case GameplayInitialization.Players:
                    Player.CreateForGameplay();
                    break;
            }
        }

        gameInitialized = true;
        if (OnGameplayInitialization != null) OnGameplayInitialization.Invoke();
    }
}
