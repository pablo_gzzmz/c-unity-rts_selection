
using UnityEngine;
using UnityEngine.EventSystems;
using Gameplay;

public class InputController : Singleton<InputController> {

    // Double clicking
    private static float doubleClickTimer;
    public  static bool  isDoubleClicking { get { return doubleClickTimer < Config.DoubleClickTime; } }

    private void Update() {
        doubleClickTimer += Time.unscaledDeltaTime;

        MouseInput();
    }
    
    private void MouseInput() {
        if (!EventSystem.current.IsPointerOverGameObject() || SelectionManager.IsMouseDown) {
            // Left click
            if (Input.GetMouseButtonDown(0)) {
                /*
                Add poossible conditions before selection

                if 
                else*/
                    SelectionManager.MouseDown();
            }

            else if (Input.GetMouseButtonUp(0)) {
                SelectionManager.MouseUp(isDoubleClicking);

                if (!isDoubleClicking) doubleClickTimer = 0;
            }

            // Right click
            if (Input.GetMouseButtonDown(1)) {
                /*
                Possible interaction commands for player's selection

                // Interaction command
                if (SelectionManager.IsCursorHovering) {
                    ISelectable hovered = SelectionManager.CursorHovered;

                    for (int i = 0; i < LocalPlayer.Selection.Count; i++) {
                        LocalPlayer.Selection[i].IssueCommand(new InteractionCommand(hovered));
                    }
                }

                // Position command
                else {
                    for (int i = 0; i < LocalPlayer.Selection.Count; i++) {
                        // This should be changed for view camera raycasting ground calculation
                        LocalPlayer.Selection[i].IssueCommand(new PositionCommand(Vector3.zero));
                    }
                }
                */
            }
        }
    }
}
