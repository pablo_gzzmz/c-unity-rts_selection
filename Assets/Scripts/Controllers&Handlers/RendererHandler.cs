
using UnityEngine;

/// <summary>
/// Component class to handle common render-related functionalities.
/// </summary>

public class RendererHandler : MonoBehaviour {

    [SerializeField]
    protected new Renderer renderer;
    public        Renderer Renderer { get { return renderer; } }

    protected Material originalMaterial;
    public    Material OriginalMaterial { get { return originalMaterial; } }

    protected Color originalColor;
    public    Color OriginalColor { get { return originalColor; } set { originalColor = value; SetColor(value); } }

    protected Color currentColor;
    public    Color CurrentColor { get { return currentColor; } }

    protected float flashTimer;
    protected float flashDuration;
    protected Color flashColor;
    protected bool  colorFlashing = false;

    protected bool destroyed = false;

    protected virtual void Awake() {
        Debug.Assert(renderer != null, "This handler requires the specification of a serialized renderer to work.");

        Show();

        originalColor = renderer.material.color;
        currentColor = originalColor;
        originalMaterial = renderer.material;
    }

    protected virtual void Update() {
        if (colorFlashing) {
            float t = flashTimer / flashDuration;
            renderer.material.color = Color.Lerp(flashColor, currentColor, t);
            flashTimer += Time.unscaledDeltaTime;

            if (flashTimer >= flashDuration) {
                colorFlashing = false;
                renderer.material.color = currentColor;
            }
        }
    }

    protected virtual void OnDestroy() {
        destroyed = true;
    }
    
    public void SetColor(Color newColor) {
        if (newColor == currentColor) return;

        if (!colorFlashing && renderer != null) {
            renderer.material.color = newColor;
        }

        currentColor = newColor;
    }
    
    public void ColorFlash(Color color, float duration, bool interruptCurrent = true) {
        if (!interruptCurrent && colorFlashing) return;

        colorFlashing = true;
        flashDuration = duration;
        flashTimer    = 0;
        flashColor    = color;
    }

    public void SetMaterial(Material mat) {
        renderer.material = mat;
    }

    public void SetOriginalMaterial() {
        renderer.material = originalMaterial;
    }

    public void Hover() {
        SetColor(Color.green);
    }

    public void Unhover() {
        SetColor(originalColor);
    }

    public void Hide() {
       renderer.enabled = false;
    }

    public void Show() {
       renderer.enabled = true;
    }

    public bool IsInCameraScreen() {
        if (destroyed || !renderer.isVisible) return false;

        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);

        //If bounds are inside main camera frustrum
        return GeometryUtility.TestPlanesAABB(planes, renderer.bounds) ? true : false;
    }
}
