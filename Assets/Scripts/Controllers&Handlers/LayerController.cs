
using UnityEngine;

/// <summary>
/// Contains all layer masks and values for easy access and readability.
/// 
/// For this simple project only one layer is necessary.
/// </summary>

public static class LayerController{

    private static LayerMask selectableMask;
    public  static LayerMask SelectableMask { get { return selectableMask; } }
    private static int selectableValue;
    public  static int SelectableValue { get { return selectableValue; } }

    static LayerController() {
        selectableValue = LayerMask.NameToLayer("Selectable");
        selectableMask  = 1 << selectableValue;
    }
}
