
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace Gameplay {

    /// <summary>
    /// Interface for all game world selectable elements. 
    /// </summary>

    public interface ISelectable {
    
        bool Selected { get; }

        int UniqueID { get; }
        int PlayerID { get; }
        int TeamID   { get; }
        int TypeID   { get; }

        float SelectionRadius   { get; }
        bool  OnScreen          { get; }
        bool  HasGroupSelection { get; }

        int SelectionPriority { get; }

        Transform Transform { get; }

        void Select();
        void Deselect();

        void Hover();
        void Unhover();
    }
}