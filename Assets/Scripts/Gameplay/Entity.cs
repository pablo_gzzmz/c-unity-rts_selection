
using UnityEngine;
using System.Collections.Generic;
using System;
using Gameplay;

/// <summary>
/// Basic component for any gameworld entity created as an instantiated 3D gameobject. Used as a simple placeholder to test ISelectable interface.
/// </summary>

[RequireComponent(typeof(RendererHandler))]
public class Entity : PlayerBoundComponent, ISelectable {
    
    public EntityCategory Category { get { return data.Category; } }

    [SerializeField]
    protected EntityData data;
    public    EntityData Data { get { return data; } }

    [SerializeField]
    [Range(0, Globals.playerMax)]
    protected int playerID;
    public    int PlayerID { get { return player.ID; } }

    [SerializeField]
    [Tooltip("Image used to notify of hovering and selection")]
    private SpriteRenderer selectionImg;

    protected RendererHandler rendererHandler;
    public    RendererHandler RendererHandler { get { return rendererHandler; } }
    
    protected bool selected;
    public    bool Selected  { get { return selected; } }

    private int selectionPriority;
    public  int SelectionPriority { get { return selectionPriority; } }

    public bool HasGroupSelection { get { return data.HasGroupSelection; } }
    
    protected int uniqueID;
    public    int UniqueID { get { return uniqueID;      } }
    public    int TeamID   { get { return player.Team;   } }
    public    int TypeID   { get { return data.NameHash; } }
    
    public  float SelectionRadius { get { return data.SelectionRadius; } }

    public bool OnScreen { get { return rendererHandler.IsInCameraScreen(); } }

    public Transform Transform { get { return transform; } }

    private bool destroyed;
    public  bool Destroyed { get { return destroyed; } }
    public event Action OnDestruction;

    private void OnValidate() {
        if (Application.isPlaying && InitializationController.GameInitialized) {
            SetPlayer(playerID);
            SetChanges();
        }
    }

    protected virtual void Start() {
        Debug.Assert(InitializationController.GameInitialized, "Gameplay initialization not detected; prephaps this object is in a non-gameplay scene.");

        SetPlayer(playerID);
        uniqueID = GetInstanceID();

        rendererHandler = GetComponent<RendererHandler>();
        SetChanges();
        player.OnChanges += SetChanges;
        
        if (data.IsSelectable) {
            Selection.AddSelectable(this);
            Deselect();
        }
    }

    protected virtual void OnDestroy() {
        if (data.IsSelectable) Selection.RemoveSelectable(uniqueID);

        destroyed = true;
        if (OnDestruction != null) OnDestruction.Invoke();
    }

    private void SetChanges() {
        rendererHandler.SetColor(Player.Profile[PlayerID].color);
        SetSelectionPriority();
    }

    public virtual void Hover  () { if (!selected) selectionImg.enabled = true;  } 
    public virtual void Unhover() { if (!selected) selectionImg.enabled = false; }

    public virtual void Select  () {
        selected = true;
        rendererHandler.ColorFlash(Color.black, 0.25f);

        selectionImg.enabled = true;
    }

    public virtual void Deselect() {
        selected = false;

        selectionImg.enabled = false;
    }

    private void SetSelectionPriority() {
        selectionPriority = LocalPlayer.GetSelectionPriority(this);
        selectionImg.color = Globals.selectionPriorityColors[selectionPriority];
    }

    public bool ChangePlayer(int playerIndex) {
        if (playerIndex == player.ID) return false;

        Deselect();
        SetPlayer(playerIndex);
        SetChanges();

        return true;
    }
}