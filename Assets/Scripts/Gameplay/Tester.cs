
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gameplay;

public class Tester : MonoBehaviour {
    
    // Simple testing
	private void Update () {
        ISelectable s = SelectionManager.CursorHovered;

        if (Input.GetKeyDown(KeyCode.Alpha1)) {
            if (s != null) {
                s.Transform.GetComponent<Entity>().ChangePlayer(1);
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha2)) {
            if (s != null) {
                s.Transform.GetComponent<Entity>().ChangePlayer(2);
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha3)) {
            if (s != null) {
                s.Transform.GetComponent<Entity>().ChangePlayer(3);
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha4)) {
            if (s != null) {
                s.Transform.GetComponent<Entity>().ChangePlayer(4);
            }
        }

        if (Input.GetKeyDown(KeyCode.P)) {
            if (s != null) {
                UIFloatingText.Log("Priority: " + (s.SelectionPriority + 1), s.Transform.position, Player.Get(s.PlayerID).Color);
            }
        }

        if (Input.GetKeyDown(KeyCode.G)) {
            if (s != null) {
                string text = s.HasGroupSelection ? "Can group" : "Cannot group";

                UIFloatingText.Log(text, s.Transform.position, Color.white);
            }
        }
	}
}
