
using System;
using UnityEngine;

/// <summary>
/// Scriptable Object to serialize Selectable Entities' data.
/// </summary>

[CreateAssetMenu(fileName = "Some Entity", menuName = "Database/Placeholder Entity")]
public class EntityData : Dataholder {

    [Space(16)]
    [Header("Entity")]
    [SerializeField]
    private         EntityCategory category;
    public override EntityCategory Category { get { return category; } }

    [SerializeField]
    private bool isSelectable = true;
    public  bool IsSelectable { get { return isSelectable; } }

    [Space(12)]
    [SerializeField]
    private float selectionRadius = 10;
    public  float SelectionRadius { get { return selectionRadius; } }

    [Space(12)]
    [SerializeField]
    private bool hasGroupSelection;
    public  bool HasGroupSelection { get { return hasGroupSelection; } }
}