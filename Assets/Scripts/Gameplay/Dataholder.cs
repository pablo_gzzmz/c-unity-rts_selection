
using UnityEngine;

/// <summary>
/// Abstract scriptable object to depict the basic data of any dataholder.
/// </summary>

public abstract class Dataholder : ScriptableObject {
    
    public string Name { get { return name; } }

    private int nameHash;
    public  int NameHash { get { return nameHash; } }

    public abstract EntityCategory Category { get; }

    [Multiline]
    [SerializeField]
    protected string description;
    public    string Description { get { return description; } }

    [SerializeField]
    protected Sprite icon = null;
    public    Sprite Icon { get { return icon; } }
    
    public virtual Color identifierColor { get { return Color.white; } }
    
    protected virtual void Awake() {
        nameHash = name.GetHashCode();
    }
}
