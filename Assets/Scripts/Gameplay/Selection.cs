
using System;
using System.Collections.Generic;

namespace Gameplay {

    /// <summary>
    /// Simple class to handle selected objects.
    /// </summary>

    public class Selection {
        
        public ISelectable[] selected = new ISelectable[Globals.selectedMax];

        private int count;
        public  int Count { get { return count; } set { count = value; } }

        public ISelectable this[int i] { get { return selected[i]; } set { selected[i] = value; } }

        public bool Remove(ISelectable s) {
            int i = 0;
            bool success = false;
            for (; i < selected.Length; i++) {
                if (selected[i].UniqueID == s.UniqueID) {
                    selected[i] = null;
                    success = true;
                    break;
                }
            }

            if (!success) return false;

            count--;

            if (count > 0) {
                for (; i < selected.Length; i++) {
                    selected[i - 1] = selected[i];
                }
            }
            return true;
        }
    
        // Static control--
        private static Dictionary<int, ISelectable> allSelectables = new Dictionary<int, ISelectable>();
        public  static Dictionary<int, ISelectable> AllSelectables { get { return allSelectables; } }

        public  static void AddSelectable(ISelectable element) {
            allSelectables.Add (element.UniqueID, element);
        }

        public  static bool RemoveSelectable(int id) {
            ISelectable s = allSelectables[id];
            if (OnSelectableRemoved != null) OnSelectableRemoved.Invoke(s);
            return allSelectables.Remove(id);
        }

        public static event Action<ISelectable> OnSelectableRemoved;
    }
}
