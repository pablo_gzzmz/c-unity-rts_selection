
using UnityEngine;
using Gameplay;

/// <summary>
/// Simple placeholder version of a local player script.
/// This script or a similar one should always be a dependency for the selection manager.
/// </summary>

[RequireComponent(typeof(InputController))]
[RequireComponent(typeof(SelectionManager))]
public class LocalPlayer : PlayerBoundSingleton<LocalPlayer> {

    private const string goName = "Local Player";

    private static Selection selection;
    public  static Selection Selection { get { return selection; } }

    public static LocalPlayer Create(int playerIndex) {
        Debug.Assert(GameObject.Find(goName) == null);

        GameObject go = new GameObject(goName);

        LocalPlayer component = go.AddComponent<LocalPlayer>();
        SetPlayer(playerIndex);
        
        selection = new Selection();

        return component;
    }

    /// <summary>
    /// Method to establish the correct selection priority from the point of view of the local player
    /// </summary>
    public static int GetSelectionPriority(ISelectable selectable) {
        // Player
        if (selectable.PlayerID == player.ID) {
            return selectable.HasGroupSelection ? 0 : 1;
        }
        // Ally team
        else if (selectable.TeamID == player.Team) {
            return selectable.HasGroupSelection ? 2 : 3;
        }
        // Neutral
        else if (selectable.PlayerID == Player.Neutral) {
            return selectable.HasGroupSelection ? 4 : 5;
        }
        // Enemy
        else {
            return selectable.HasGroupSelection ? 6 : 7;
        }
    }
}
