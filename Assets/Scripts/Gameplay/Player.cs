
using UnityEngine;
using System;
using System.Collections.Generic;

/// <summary>
/// A simple placeholder class to simulate a player.
/// </summary>

public class Player {

    private bool active;
    public  bool Active { get { return active; } }

    private int id;
    public  int ID { get { return id; } }

    private int team;
    public  int Team { get { return team; } set { team = Mathf.Clamp(value, 1, Globals.playerMax); } }
    
    private Color color;
    public  Color Color { get { return color; } }

    public  event Action OnChanges;

    private Player(int _id) { id  = _id; }

    private void Initialize() {
        active = true;
        // ...
    }

    private void Remove() {
        active = false;
        // ...
    }

    public void Set(PlayersProfile.PlayerInfo info) {
        color = info.color;
        team  = info.team;
        if (OnChanges != null) OnChanges.Invoke();
    }

    // Static control--
    
    public  const  int Neutral = 0;
    private static int local;

    private static PlayersProfile profile;
    public  static PlayersProfile Profile { get { return profile; } }

    static Player (){
        profile = Resources.Load<PlayersProfile>("Profiles/Players Profile");
        profile.OnValidateCompleted += SetPlayerByProfile;

        local = 1;

        for (int i = 0; i < players.Length; i++) {
            players[i] = new Player(i);
        }

        SetPlayerByProfile();
    }
    
    private static Player[] players = new Player[Globals.playerMax+1];
    public  static Player Get(int i) {
        if (i > Globals.playerMax) {
            Debug.LogWarning("Invalid player index requested. Returning neutral player.");
            return players[0];
        }

        if (!players[i].active) {
            Debug.Log("Requested inactive player. Initializing new player...");
            players[i].Initialize();
            return players[i];
        }

        return players[i];
    }

    public static void AddPlayer(int index) {
        if (index < 1) {
            Debug.LogWarning("Attempt at removing neutral player.");
            return;
        }

        players[index].Initialize();
    }

    public static void RemovePlayer(int index) {
        if (index == 0) return;

        players[index].Remove();
    }

    public static void CreateForGameplay() {
        // for (int i = 1; i < players.Count; i++) {}

        Debug.Assert(local > 0 && local < players.Length);

        LocalPlayer.Create(local);
    }

    public static void DisposeGameplay() {
        UnityEngine.Object.Destroy(LocalPlayer.Instance.gameObject);
    }

    private static void SetPlayerByProfile() {
        for (int i = 0; i < players.Length; i++) {
            players[i].Set(profile[i]);
        }
    }
}
