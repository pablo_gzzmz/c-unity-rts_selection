
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Collections.Generic;
using GLPainting;

namespace Gameplay {

    /// <summary> 
    /// Singleton that takes care of player selection using click selection, box selection and type selection of objects.
    /// </summary>

    public class SelectionManager : Singleton<SelectionManager> {

        private const float clickZone = 4f; // Threshold distance prior to drag - screen size
        private const float boxHoverRefreshRate = 0.05f;

        // Box selection is performed with an array of hash sets, containing selectable object IDs for each selection priority
        private static HashSet<int>[] hovered = new HashSet<int>[Globals.selectionPriorities];
        private static int hoveredCount;

        // For default single cursor hovering
        private static ISelectable cursorHovered;
        public  static ISelectable CursorHovered  { get { return cursorHovered; } }

        private static bool isCursorHovering = false;
        public  static bool IsCursorHovering { get { return isCursorHovering; } }

        private static Collider lastHit;

        private static Vector3 mouse;

        private static bool isMouseDown;
        public  static bool IsMouseDown { get { return isMouseDown; } }
        private static bool wasDragging; // To identify end and start of dragging
        private static bool isDragging  { get { return DetectDragging(mouseDragStart, mouse) && isMouseDown; } }

        // Selection box
        private static Vector2 mouseDragStart;
        private static Vector2 boxStart;
        private static Vector2 boxFinish;
        private static float boxWidth;
        private static float boxHeight;

        private static bool isBoxHovering;
        private static bool attemptBoxHoverSelection;
        private static float boxHoverRefreshTimer;

        public static event Action OnSelection;
        public static event Action OnDeselection;

        // Selection box GL visual display
        private static GLDrawLine[] glLines = new GLDrawLine[2];

        private void Start() {
            CreateGLLines();

            // Create hash sets
            for (int i = 0; i < hovered.Length; i++) hovered[i] = new HashSet<int>();

            // Ensure hash sets are corrected if a selectable is removed/deleted
            Selection.OnSelectableRemoved += FixHoveredAfterRemoval;
        }

        private void OnDestroy() {
            Selection.OnSelectableRemoved -= FixHoveredAfterRemoval;
        }

        private static void  CreateGLLines() {
            // Outline
            glLines[0] = new GLDrawLine(Vector2.zero, Vector2.zero, Globals.selectionColor, GLDrawLine.Category.Box);
            glLines[0].draw = false;
            // Background quad
            Color backgroundColor = Globals.selectionColor;
            backgroundColor.a = 0.2f;
            glLines[1] = new GLDrawLine(Vector2.zero, Vector2.zero, backgroundColor, GLDrawLine.Category.Quad);
            glLines[1].draw = false;
            GLPainter.AddLine(glLines[1]);
            GLPainter.AddLine(glLines[0]);
        }

        // Fix for hovered selectable objects removed while box hovering
        private static void FixHoveredAfterRemoval(ISelectable s) {
            if (isDragging && hovered[s.SelectionPriority].Remove(s.UniqueID)) {
                hoveredCount--;
            }
        }

        private void Update() {
            mouse = Input.mousePosition;

            if (isDragging)     SetHoverBox();
            
            if (isBoxHovering)  PerformHoverBox();
            
            else                DefaultCursorHovering();


            DraggingControl();
        }

        private static void SetHoverBox() {
            boxStart = mouse;
            boxFinish = mouse;
            boxWidth = mouse.x - mouseDragStart.x;
            boxHeight = mouseDragStart.y - mouse.y;

            if (boxWidth > 0f && boxHeight < 0f) /* TOP RIGHT    */ boxStart = new Vector2(mouse.x - boxWidth, mouse.y);
            else
            if (boxWidth > 0f && boxHeight > 0f) /* BOTTOM RIGHT */ boxStart = new Vector2(mouse.x - boxWidth, mouse.y + boxHeight);
            else
            if (boxWidth < 0f && boxHeight < 0f) /* TOP LEFT     */ boxStart = mouse;
            else
            if (boxWidth < 0f && boxHeight > 0f) /* BOTTOM LEFT  */ boxStart = new Vector2(mouse.x, mouse.y + boxHeight);

            boxFinish = new Vector2(boxStart.x + Mathf.Abs(boxWidth), boxStart.y - Mathf.Abs(boxHeight));

            glLines[0].ScreenSpaceSet(boxStart, boxFinish);
            glLines[1].ScreenSpaceSet(boxStart, boxFinish);
        }

        private static void PerformHoverBox() {
            // Refresh rate is used to avoid processing data every frame, yet process the latest correct data upon mouse release

            boxHoverRefreshTimer -= Time.unscaledDeltaTime;

            if (boxHoverRefreshTimer <= 0) {
                boxHoverRefreshTimer = boxHoverRefreshRate;

                foreach (int i in Selection.AllSelectables.Keys) {
                    ISelectable selectable = Selection.AllSelectables[i];

                    if (!selectable.OnScreen) continue;

                    int pIndex = selectable.SelectionPriority;

                    // Calculate position of transform with radius as upwards offset
                    // This is done to compensate for transform's position being on the ground and not centered
                    Vector3 fixedPos = selectable.Transform.position + selectable.Transform.up * selectable.SelectionRadius;

                    // Calculate world position of fixed position with radius as 'right' offset considering camera transform
                    Vector3 radiusAddedPos = fixedPos + Camera.main.transform.right * selectable.SelectionRadius;

                    // Set as screen position
                    Vector2 pos  = Camera.main.WorldToScreenPoint(fixedPos);
                    Vector2 pos2 = Camera.main.WorldToScreenPoint(radiusAddedPos);
                    
                    // Set the screen radius as the distance between the two positions
                    float radius = (pos2 - pos).magnitude;

                    // Check if the selectable is inside the hovering box
                    if ((pos.x + radius > boxStart.x  && pos.y - radius < boxStart.y) &&
                        (pos.x - radius < boxFinish.x && pos.y + radius > boxFinish.y)) {
                        if (!hovered[pIndex].Contains(i)) {
                            hovered[pIndex].Add(i);
                            hoveredCount++;

                            selectable.Hover();
                        }
                    }
                    else {
                        if (hovered[pIndex].Contains(i)) {
                            hovered[pIndex].Remove(i);
                            hoveredCount--;

                            selectable.Unhover();
                        }
                    }
                }

                // If this refresh is after signaling a selection, select and stop box-hovering
                if (attemptBoxHoverSelection) {
                    attemptBoxHoverSelection = false;
                    SelectByHoverBox();
                    isBoxHovering = false;
                }
            }
        }

        private static void DefaultCursorHovering() { 
            // Prevent functionality if cursor is hovering UI
            if (EventSystem.current.IsPointerOverGameObject()) {
                if (isCursorHovering) cursorHovered.Unhover();

                lastHit = null;
                cursorHovered = null;
                isCursorHovering = false;
            }
            else {
                RaycastHit hit;
                Ray hoverRay = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(hoverRay, out hit, Mathf.Infinity, LayerController.SelectableMask)) {
                    if (lastHit != hit.collider) {
                        if (cursorHovered != null) cursorHovered.Unhover();

                        // A selectable component should always be in the parent object
                        cursorHovered = hit.collider.transform.parent.GetComponent<ISelectable>();

                        if (cursorHovered != null) {
                            cursorHovered.Hover();
                            isCursorHovering = true;
                        }
                        else isCursorHovering = false;
                    }

                    lastHit = hit.collider;
                }
                else {
                    if (isCursorHovering) cursorHovered.Unhover();

                    lastHit = null;
                    cursorHovered = null;
                    isCursorHovering = false;
                }
            }
        }

        // Performs basic functionality on dragging starts / on dragging ends
        private static void DraggingControl() {
            if (wasDragging != isDragging) {
                // On stopped dragging
                if (wasDragging) {
                    glLines[0].draw = false;
                    glLines[1].draw = false;
                }

                // On began dragging
                else {
                    glLines[0].draw = true;
                    glLines[1].draw = true;
                    if (isCursorHovering) cursorHovered.Unhover();

                    isBoxHovering = true;
                    boxHoverRefreshTimer = 0;
                }
            }

            wasDragging = isDragging;
        }
        

        public  static void MouseDown() {
            // Drag attempt initialization
            mouseDragStart = Input.mousePosition;
            isMouseDown = true;
            
            for (int i = 0; i < Globals.selectionPriorities; i++) {
                hovered[i].Clear();
            }

            hoveredCount = 0;

            wasDragging = false;
        }

        public  static void MouseUp(bool doubleClick) {
            // Box selection
            if (isDragging) attemptBoxHoverSelection = true;

            // Cursor selection
            else if (isCursorHovering && cursorHovered != null) {
                if (doubleClick && cursorHovered == LocalPlayer.Selection[0]) {
                    SelectByType();
                }
                else {
                    Deselect();
                    Select(cursorHovered, 0);
                    FinalizeSelection(1);
                }
            }

            isMouseDown = false;
        }


        // Performs the process of box selecting after a box hovering has finalized
        private static void SelectByHoverBox() {
            if (hoveredCount > 0) {
                Deselect();

                int count = 0;
                int validPriority = -1;

                for (int currentPriority = 0; currentPriority < Globals.selectionPriorities; currentPriority++) {
                    if (hovered[currentPriority].Count == 0) continue;

                    // Store the valid priority as soon as we find one Hash Set with elements
                    if (validPriority == -1) validPriority = currentPriority;

                    foreach (int i in hovered[currentPriority]) {
                        // All elements need to be unhovered, so we canot break from any loop
                        ISelectable element = Selection.AllSelectables[i];
                        element.Unhover();

                        if (count == Globals.selectedMax) continue;
                        
                        if (validPriority == currentPriority) {
                            if (element.HasGroupSelection || count == 0) {
                                Select(element, count);
                                count++;
                            }
                        }
                    }
                }

                if (count > 0) {
                    FinalizeSelection(count);
                }
            }
        }
        
        // Selects all elements on screen of the same type as long as they can be selected in group and belong to the same player
        private static bool SelectByType() {
            if (cursorHovered != null && cursorHovered.Selected && cursorHovered.HasGroupSelection) {
                int index    = cursorHovered.TypeID;
                int playerID = cursorHovered.PlayerID;

                int count = 1;
                foreach(int i in Selection.AllSelectables.Keys) {
                    ISelectable s = Selection.AllSelectables[i];
                    
                    // Look for valid selectables
                    if (s.OnScreen && s.TypeID == index && s.PlayerID == playerID) {
                        if (count == Globals.selectedMaxIndex) break;
                        
                        Select(s, count);
                        count++;
                    }
                }
                FinalizeSelection(count);
                return true;
            }
            return false;
        }

        // Selects a single selectable - requires further finalization
        private static void Select(ISelectable s, int index) {
            s.Select();
            
            LocalPlayer.Selection[index] = s;
        }

        // Deselects all selectables, clearing selection
        private static void Deselect() {
            for (int i = 0; i < LocalPlayer.Selection.Count; i++) {
                LocalPlayer.Selection[i].Deselect();
                LocalPlayer.Selection[i] = null;
            }

            LocalPlayer.Selection.Count = 0;
            if (OnDeselection != null) OnDeselection.Invoke();
        }

        // Finalizes a selection, indicating the resulting count
        private static void FinalizeSelection(int count) {
            LocalPlayer.Selection.Count = count;
            if (OnSelection != null) OnSelection.Invoke();
        }


        private static bool DetectDragging(Vector2 dragStartPoint, Vector2 newPoint) {
            if ((newPoint.x > dragStartPoint.x + clickZone || newPoint.x < dragStartPoint.x - clickZone) &&
                (newPoint.y > dragStartPoint.y + clickZone || newPoint.y < dragStartPoint.y - clickZone))
                return true;
            else
                return false;
        }
    }
}
