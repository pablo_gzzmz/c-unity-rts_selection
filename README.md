# README #
#
Author: Pablo González Martínez
#
pablogonzmartinez@gmail.com

Unity3D Engine
C#

This project  showcases an implementation of a selection system imitating RTS games.

# Google Docs doc #
https://docs.google.com/document/d/1nOyOx9H3CrpuVIueMl7ad3zTUMDsS7Btp-7iuajoDWA/edit?usp=sharing

# Requirements #
The project has been created with Unity 2017.1.1f1 (64-bit).

# After-download guide: #
It contains no .exe, as the execution of the project itself ignoring the code is not very relevant. It should be opened in Unity using the parent folder containing ‘Assets’ and ‘Project Settings’ folders.

The project is organized in folders. The main scene is labelled as ‘test_gameplay’, and is contained in the Scenes folder.

All the code is contained in the Scripts folder and has been created exclusively by the author.
